// Gulp.js configuration
'use strict';

const
 main = {
    themefolder : 'molensteen-2019'
 };

const

  // source and build folders
  dir = {
    src         : 'assets/',
    build       : '../wp/wp-content/themes/' + main.themefolder + '/'
  },

  // Gulp and plugins
  gulp          = require('gulp'),
  gutil         = require('gulp-util'),
  newer         = require('gulp-newer'),
  imagemin      = require('gulp-imagemin'),
  sass          = require('gulp-sass'),
  postcss       = require('gulp-postcss'),
  deporder      = require('gulp-deporder'),
  concat        = require('gulp-concat'),
  stripdebug    = require('gulp-strip-debug'),
  uglify        = require('gulp-uglify')
;

// Browser-sync
var browsersync = false;

gulp.task('cachebust', function() {
  gulp.src('../wp/wp-content/themes/' + main.themefolder + '/includes/versioning.php', {base: './'})
      .pipe(wpcachebust({
        themeFolder: '../wp/wp-content/themes/' + main.themefolder + '',
        rootPath: './'
      }))
      .pipe(gulp.dest('./'))
  }
);

// PHP settings
const php = {
  src           : dir.src + 'template/**/*.php',
  build         : dir.build
};

// copy PHP files
gulp.task('php', function() {
  return gulp.src(php.src)
    .pipe(newer(php.build))
    .pipe(gulp.dest(php.build));
});


// FONT settings
const fonts = {
  src         : dir.src + 'fonts/**/*',
  build       : dir.build + 'assets/fonts/'
};

// copy FONT files
gulp.task('fonts', () => {
  return gulp.src(fonts.src)
    .pipe(newer(fonts.build))
    .pipe(gulp.dest(fonts.build));
});


// image settings
const images = {
  src         : dir.src + 'images/**/*',
  build       : dir.build + 'assets/images/'
};

// image processing
gulp.task('images', function() {
  return gulp.src(images.src)
    .pipe(newer(images.build))
    .pipe(imagemin())
    .pipe(gulp.dest(images.build));
});


// CSS settings
var css = {
  src         : dir.src + 'sass/style.scss',
  watch       : dir.src + 'sass/**/*',
  build       : dir.build + 'assets/css/' ,
  sassOpts: {
    outputStyle     : 'nested',
    imagePath       : images.build,
    precision       : 3,
    errLogToConsole : true
  },
  processors: [
    require('postcss-assets')({
      loadPaths: ['images/'],
      basePath: dir.build,
      baseUrl: '/wp-content/themes/' + main.themefolder + '/'
    }),
    require('autoprefixer')({
      browsers: ['last 2 versions', '> 2%']
    }),
    require('css-mqpacker'),
    require('cssnano')
  ]
};

// CSS processing
gulp.task('css', gulp.parallel('images', function() {
    return gulp.src([css.src],[css.watch])
      .pipe(sass(css.sassOpts))
      .pipe(postcss(css.processors))
      .pipe(gulp.dest(css.build))
      .pipe(browsersync ? browsersync.reload({ stream: true }) : gutil.noop());
  })
);


// JavaScript settings
const js = {
  src         : dir.src + 'js/**/*',
  build       : dir.build + 'assets/js/',
  filename    : 'scripts.js'
};

// JavaScript processing
gulp.task('js', function() {

  return gulp.src(js.src)
    .pipe(deporder())
    .pipe(concat(js.filename))
    //.pipe(stripdebug())
    .pipe(uglify())
    .pipe(gulp.dest(js.build))
    .pipe(browsersync ? browsersync.reload({ stream: true }) : gutil.noop());
});

const wpcachebust = require('gulp-wp-cache-bust');

gulp.task('cachebust', function() {
  gulp.src('../wp/wp-content/themes/' + main.themefolder + '/includes/versioning.php', {base: './'})
      .pipe(wpcachebust({
        themeFolder: '../wp/wp-content/themes/' + main.themefolder + '',
        rootPath: './'
      }))
      .pipe(gulp.dest('../'))
  }
);

gulp.task('build', gulp.parallel('fonts', 'css', 'js', 'php'));


// Browsersync options
const syncOpts = {
  proxy       : 'localhost',
  files       : dir.build + '**/*',
  open        : false,
  notify      : false,
  ghostMode   : false,
  ui: {
    port: 8001
  }
};


// browser-sync
gulp.task('browsersync', function() {
  if (browsersync === false) {
    browsersync = require('browser-sync').create();
    browsersync.init(syncOpts);
  }

});

// watch for file changes
gulp.task('watch', gulp.parallel('browsersync', function() {

  // page changes
  //gulp.watch(php.src, ['php'], browsersync ? browsersync.reload : {});
  gulp.watch(php.src, gulp.series('php'));

  // image changes
  gulp.watch(images.src, gulp.series('images'));

  // font changes
  gulp.watch(fonts.src, gulp.series('fonts'));

    // CSS changes
  gulp.watch([css.src,css.watch], gulp.series('css'));

  // JavaScript main changes
  gulp.watch(js.src, gulp.series('js'));


}));


gulp.task('default', gulp.parallel('build', 'watch'));
