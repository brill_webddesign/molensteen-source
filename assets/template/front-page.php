<?php get_header() ?>

<?php while ( have_posts() ) : the_post(); ?>

<main>
    <div class="ms-c-header-slider animatedParent">
    <div class="owl-carousel  ms-c-carousel animated fadeIn" id="header_carousel">

            <?php if ( have_rows( 'slides' ) ) : ?>

                <?php while ( have_rows( 'slides' ) ) : the_row(); ?>

                    <?php $slide_image = get_sub_field( 'slide_image' ); ?>

                    <div class="item  animated fadeIn d-flex  align-items-end  ms-c-header-slider__item  embed-responsive  embed-responsive-18by9" style="background-image: url('<?php echo $slide_image['url']; ?>')">


                        <div class="ms-c-header-slider__item--content animated fadeInUpShort">


                	       <h2 class="ms-u-header-slider__title" ><?php the_sub_field( 'slide_title' ); ?></h2>

                        <?php if ( get_sub_field( 'slide_button' ) == 1 ) :  ?>

                            <a href="<?php the_sub_field( 'slide_button_link' ); ?>" class="ms-c-button--header"><?php the_sub_field( 'slide_button_text' ); ?></a>

            		    <?php endif; ?>

                    </div> <!-- slide item content -->

                </div> <!-- slide item -->

                <?php endwhile; ?>

            <?php endif; ?>

        </div>

        <ul class="ms-c-owl-dots">
            <?php while ( have_rows( 'slides' ) ) : the_row(); ?>
                <li class="<?php if ( get_row_index () == 1 ) { echo "active"; } ?>" data="<?php echo get_row_index() ?>"><span></span></li>
            <?php endwhile; ?>
        </ul>
    </div>

    <div class="container animatedParent" data-sequence="200">

        <div class="row  text-center">
            <div class="col-9  mx-auto ms-c-col-home col-md-4 animated fadeInDownShort" data-id="1">

                <?php while ( have_rows( 'frontpage_column_1' ) ) : the_row(); ?>

                    <h2 class="ms-u-text-color--terracotta"><i  class="ms-a-icon--heerlijk"></i><?php the_sub_field( 'frontpage_intro_column_title' ); ?></h2>
                	<?php the_sub_field( 'frontpage_intro_column_text' ); ?>

            	<?php endwhile; ?>
            </div>
            <div class="col-9  mx-auto ms-c-col-home col-md-4 animated fadeInDownShort" data-id="2">

                <?php while ( have_rows( 'frontpage_column_2' ) ) : the_row(); ?>

                    <h2 class="ms-u-text-color--manhattan"><i  class="ms-a-icon--eerlijk"></i> <?php the_sub_field( 'frontpage_intro_column_title' ); ?></h2>
                    <?php the_sub_field( 'frontpage_intro_column_text' ); ?>

            <?php endwhile; ?>
            </div>
            <div class="col-9  mx-auto ms-c-col-home col-md-4 animated fadeInDownShort" data-id="3">


                <?php while ( have_rows( 'frontpage_column_3' ) ) : the_row(); ?>

                    <h2 class="ms-u-text-color--rajah"><i  class="ms-a-icon--eenvoudig"></i> <?php the_sub_field( 'frontpage_intro_column_title' ); ?></h2>
                    <?php the_sub_field( 'frontpage_intro_column_text' ); ?>

                <?php endwhile; ?>

            </div>
        </div>
    </div>

    <div class="container-fluid animatedParent d-none d-md-block">


        <div class="animated fadeIn">

            <?php $page_breaker_image = get_field( 'page_breaker_image' ); ?>

            <div class="parallax-window" data-parallax="scroll"  data-bleed="0" data-image-src="<?php echo $page_breaker_image['url']; ?>" data-natural-width="<?php echo $page_breaker_image['width']; ?>" data-natural-height="<?php echo $page_breaker_image['height']; ?>"></div>

        </div>

    </div>

    <?php bd_get_product_slider( true ); ?>

    <div class="container-fluid  d-md-none  ms-u-bg--solitaire">

        <div class="container">
            <div class="row text-center">
                <div class="col-10  mx-auto">
                    <h2 class="ms-u-text-color--grey-dark">Molensteen producten</h2>
                </div>
            </div>
        </div>
        <img src="<?php bloginfo('template_url'); ?>/assets/images/ms-a-placeholder-14.png" alt="" class="">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <a href="#" class="ms-c-button ms-c-button--grey">bekijk onze complete familie</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container d-md-none  p-0 pt-3">
        <div class="row">
            <div class="col-12  text-center">
                <h2 class="ms-u-text-color--manhattan">Onze recepten</h2>
            </div>
        </div>
    </div>
    <?php if ( have_rows( 'recipe_page_breaker' ) ) : ?>
    	<?php while ( have_rows( 'recipe_page_breaker' ) ) : the_row(); ?>

            <?php $image = get_sub_field( 'image' ); ?>

            <div class="container-fluid  animatedParent ms-c-recipe-breaker" style="background-image:url('<?php echo $image['url']; ?>')">

                <div class="container  text-center">
                    <div class="row">
                        <div class="col-9 col-sm-8  col-md-6 col-lg-5  mx-auto animated fadeInLeftShort">
                            <h2 class="ms-u-text-color--white"><?php the_sub_field( 'recipe_title' ); ?></h2>
                            <?php the_sub_field( 'recipe_text' ); ?>

                        </div>
                    </div>
                </div>
            </div>

    	<?php endwhile; ?>
    <?php endif; ?>

    <div class="container-fluid d-none d-md-block ms-u-bg--solitaire animatedParent">

        <div class="container">
            <div class="row animated fadeIn">

                <div class="col-12  text-center">
                    <h2 class="ms-u-text-color--rajah">Recepten</h2>
                </div>
            </div> <!-- row -->
            <div class="row animated fadeIn">

                <?php bd_get_recipes(); ?>

            </div> <!-- row -->
            <div class="row pt-5">
                <div class="col-12  text-center">
                    <a href="<?php bloginfo('url'); ?>/recepten" class="ms-c-button">laat meer molensteenrecepten zien</a>
                </div>
            </div> <!-- row -->
        </div>
    </div>

    <div class="container-fluid  ms-u-instagram animatedParent">
        <div class="row  text-center animated fadeInUpShort">

            <?php echo do_shortcode('[instagram-feed]'); ?>
            <div class="col-12">
                <h2>Check onze Socials<span><img src="<?php bloginfo('template_url'); ?>/assets/images/ms-a-icon--arrow.svg" /></h2>
            </div>
        </div>
    </div> <!--- instagram feed --->

    <?php bd_get_partner_container('Molensteen'); ?>

<?php endwhile; ?>

<?php get_footer() ?>
