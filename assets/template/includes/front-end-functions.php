<?php

add_filter('acf/settings/remove_wp_meta_box', '__return_true');

function bd_page_blocks() {

    $containers = get_field('containers');

    if  ( $containers ) :

        foreach ($containers as $container) : ?>

            <?php //print_r( $container ); ?>

            <?php if ( $container['acf_fc_layout'] == 'container' ) : ?>

                <?php include locate_template('blocks/container.php'); ?>

            <?php endif; ?>

            <?php if ( $container['acf_fc_layout'] == 'pagebreaker' ) : ?>

                <?php include locate_template('blocks/page-breaker.php'); ?>

            <?php endif; ?>


        <?php endforeach;

    endif;

}



/*
 * generate Page Block Image
 */
function get_page_block_image( $column ) {

    $image = $column['image']['image'];

    ?>
    <!-- <pre style="">

        <?php //print_r( $image ); ?>

    </pre> -->

    <?php // if the image type is SVG/XML just output the image url without checking custom wordpress image sizes
    if( $image['subtype'] == 'svg+xml' ) {

        $image_url = $image['url'];

    } else {

        if ( $column['image']['image_size'] == "original size" ) {

            $image_url = $image['url'];

        } else {

            $size = $column['image']['custom_size'];
            $image_url = $image['sizes'][$size];

        }
    }

    ?>

    <img src="<?php echo $image_url; ?>" alt="<?php echo $image['alt'];?>"  class="<?php echo $column['image']['image_classes']; ?>"/>

    <?php

}

/*
* echo all classes from array with double white space
*/
function get_classes($array) {

    if ( $array ) {
        foreach ($array as $class ) {
            echo $class."  ";
        }
    }
}


function bd_get_partner_container($text) {

    ?>
    <div class="container-fluid  ms-u-bg--solitaire animatedParent" data-sequence="200">
        <div class="container text-center">
            <div class="row">
                <div class="col-10  col-md-12 mx-auto">
                    <h3 class="ms-u-text-color--grey-dark  pb-3"><?php echo $text; ?> vind je in de schappen van</h3>
                </div>
            </div>
            <!-- <img src="<?php bloginfo('template_url'); ?>/assets/images/ms-a-logo--eko-plaza.png" alt="" class="ms-c-partner-logo  ms-c-partner-logo__eko"> -->
            <img src="<?php bloginfo('template_url'); ?>/assets/images/ms-a-logo--albert-heijn.png" alt="" class="ms-c-partner-logo  ms-c-partner-logo__ah animated fadeInDownShort" data-id="6">
            <!-- <img src="<?php bloginfo('template_url'); ?>/assets/images/ms-a-logo--marqt.png" alt="" class="ms-c-partner-logo  ms-c-partner-logo__marqt"> -->
        </div>
    </div>
    <?php
}


function bd_get_product_slider( $with_title = false ) {
    ?>
    <div class="container-fluid  d-none d-md-block">
        <div class="container  ms-c-slider-container">
            <div class="row text-center">
                <div class="col-12">
                    <?php if ( $with_title == true ) : ?>
                        <h2 class="ms-u-text-color--terracotta">Molensteen producten</h2>
                    <?php endif; ?>
                    <div class="ms-c-product-slider-menu" id="ms_product_slider_menu">

                        <ul>
                            <?php bd_get_categories( 'category', true ); ?>
                        </ul>

                        <!-- <ul>
                            <li id="slider_menu_basismelen" class="ms-js-is-active"><a href="#basismelen">Basismelen</a><span>Een goede basis voor de lekkerste baksels.</span></li>
                            <li id="slider_menu_bakmixen"><a href="#bakmixen">Bakmixen</a><span>Een goede basis voor de lekkerste baksels.</span></li>
                            <li id="slider_menu_muesli"><a href="#">Muesli</a><span>Een goede basis voor de lekkerste baksels.</span></li>
                            <li id="slider_menu_granola"><a href="#">Granola</a><span>Een goede basis voor de lekkerste baksels.</span></li>
                            <li id="slider_menu_kids"><a href="#">Kids</a><span>Een goede basis voor de lekkerste baksels.</span></li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="owl-carousel ms-c-products-carousel" id="products_carousel">

             <?php bd_get_products( $category_id = 'all', $slider = true ); ?>

        </div>
    </div> <!-- container fluid -->
    <?php
}

add_action( 'wp_ajax_nopriv_filter_products', 'bd_filter_products' );
add_action( 'wp_ajax_filter_products', 'bd_filter_products' );

add_action( 'wp_ajax_nopriv_filter_recipes', 'bd_filter_recipes' );
add_action( 'wp_ajax_filter_recipes', 'bd_filter_recipes' );

function bd_filter_products( $category_id ) {
    $category_id = $_POST['category_id'];
    bd_get_products( $category_id );
    die();
}

function bd_filter_recipes( $category_id ) {
    $category_id = $_POST['category_id'];
    bd_get_recipes( $category_id );
    die();
}

function bd_get_products( $category_id = 'all', $slider = false ) {

    $args = array(

           'post_type' => 'product',
           'cat' => $category_id
       );
    ?>


    <?php if ( $category_id != 'all') : ?>

        <div class="col-md-10  col-lg-9  text-center  mx-auto mb-3  pb-3">
            <?php echo term_description( $category_id, 'category' ) ?>
        </div>

    <?php endif; ?>

    <?php $the_query = new WP_Query( $args ); ?>

    <?php if( $the_query->have_posts() ): ?>

    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <?php $x++; ?>

        <?php if ( $slider == false ): ?>

            <div class="col-6 col-lg-4 ms-c-product-loop-item ms-c-product-loop-item--<?php echo $x; ?>">

        <?php endif; ?>

           <?php include( locate_template('loop-products.php' ) ); ?>

        <?php if ( $slider == false ): ?>

           </div>

       <?php endif; ?>

    <?php endwhile; ?>

   <?php wp_reset_postdata(); ?>

    <?php endif;

}

function bd_get_recipes( $category_id = 'all' ) {

    if( is_front_page() ) {
        $post_limit = 3;
        $orderby = 'rand';
    } else {
        $post_limit = -1;
        $orderby = 'none';
    }

    $args = array(

           'post_type' => 'recept',
           'posts_per_page' => $post_limit,
           'orderby' => $orderby,
           'tax_query' => array(
                array(
                    'taxonomy' => 'recept_cat',
                    'terms' => $category_id
                )
            )
       );
    ?>

    <?php $the_query = new WP_Query( $args ); ?>

    <?php if( $the_query->have_posts() ): ?>

    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <?php $x++; ?>

        <div class="col-6 col-lg-4 ms-c-product-loop-item--<?php echo $x; ?>">

           <?php get_template_part('loop', 'recipes' ); ?>

       </div>

    <?php endwhile; ?>

   <?php wp_reset_postdata(); ?>

    <?php endif;

}

function bd_get_blog( $category_id = 'all' ) {

    if( is_front_page() ) {
        $post_limit = 3;
        $orderby = 'rand';
    } else {
        $post_limit = -1;
        $orderby = 'none';
    }

    $args = array(

           'post_type' => 'post',
           'orderby' => $orderby,
           'cat' => $category_id
       );
    ?>

    <?php $the_query = new WP_Query( $args ); ?>

    <?php if( $the_query->have_posts() ): ?>

    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <?php $x++; ?>

        <div class="col-6 col-lg-4 ms-c-product-loop-item--<?php echo $x; ?>">

           <?php get_template_part('loop', 'blog' ); ?>

       </div>

    <?php endwhile; ?>

   <?php wp_reset_postdata(); ?>

    <?php endif;

}



function bd_get_categories( $taxonomy, $slider = false, $blog = false ) {

    if( $blog == true ) {
        $parent = '13';
    } else {
        $parent = '14';
    }
      $args = array(
                  'taxonomy' => $taxonomy,
                  'orderby' => 'name',
                  'parent' =>  $parent,
                  'order'   => 'DESC'
              );

      $cats = get_categories($args);

      $x = 0;
      foreach($cats as $cat) {

          if ( ( $x == 0 ) && ( $slider == true ) ) {
              $class = 'ms-js-is-active';
          } else {
              $class = '';
          }
          $x++;
          ?>
         <li data-id="<?php echo $cat->term_id; ?>" id="slider_menu_<?php echo $cat->slug; ?>" class="<?php echo $class; ?>"><a href="#<?php echo $cat->slug; ?>">
              <?php echo $cat->name; ?>
         </a>
         <?php if ( $slider == true ) : ?>
             <span><?php the_field('short_description', 'term_' . $cat->term_id ) ?></span>
        <?php endif; ?>
     </li>
        <?php
      }

}


add_filter( 'gform_validation_message', 'change_message', 10, 2 );
function change_message( $message, $form ) {
    return "<div class='validation_error'>Er ging iets mis, bekijk de meldingen hieronder</div>";
}
