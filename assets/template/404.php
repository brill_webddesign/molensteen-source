<?php get_header() ?>

<?php // Create arguments to get post type


    $page = get_page_by_title( '404' );
    $id = $page->ID;

    $args = array(
        'post_type' => 'page',
        'p' => $id,
    );

 ?>

 <?php $the_query = new WP_Query( $args ); ?>

 <?php if( $the_query->have_posts() ): ?>

     <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>

         <main>

             <?php get_template_part('partials/partial', 'header' ); ?>

             <?php bd_page_blocks(); ?>

         </main>

     <?php endwhile; ?>

 <?php endif; ?>


<?php get_footer() ?>
