<?php

// Template Name: Producten

 ?>

 <?php get_header() ?>

 <?php while ( have_posts() ) : the_post(); ?>

 <main>

     <?php get_template_part('partials/partial', 'header' ); ?>


         <div class="container ms-c-products  ms-c-max-width">

             <div class="row">

                 <div class="col-12">

                     <div class="ms-c-product-slider-menu" id="ms_product_filter">
                         <ul>
                             <li data-id="all" class="ms-js-is-active"><a href="#">Alle</a></li>

                             <?php bd_get_categories( 'category' ); ?>

                         </ul>
                         <span class="ms-c-toggle-product-filter"></span>
                     </div>
                 </div>
             </div> <!--- row -->

             <?php get_template_part('product','loader'); ?>

             <div class="row" id="ms_products_container">

                 <?php bd_get_products(); ?>

             </div> <!--- row -->

         </div> <!--- container -->


     <?php bd_page_blocks(); ?>

     <?php bd_get_partner_container('Deze producten'); ?>

 </main>

<?php endwhile; ?>

<?php get_footer() ?>
