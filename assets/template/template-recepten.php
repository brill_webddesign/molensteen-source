<?php

// Template Name: Recepten

 ?>

 <?php get_header() ?>

 <?php while ( have_posts() ) : the_post(); ?>

 <main>

     <?php get_template_part('partials/partial', 'header' ); ?>

     <div class="container-fluid ms-u-bg--solitaire  ms-c-recipes">

          <div class="container ms-c-products  ms-c-max-width">

             <div class="row">

                 <div class="col-12">

                     <div class="ms-c-product-slider-menu" id="ms_recept_filter">
                         <ul>
                             <li data-id="all" class="ms-js-is-active"><a href="#">Alle</a></li>

                             <?php bd_get_categories( 'recept_cat' ); ?>

                         </ul>
                         <span class="ms-c-toggle-product-filter"></span>
                     </div>
                 </div>
             </div> <!--- row -->

             <?php get_template_part('product','loader'); ?>

             <div class="row" id="ms_products_container">

                 <?php bd_get_recipes(); ?>

             </div> <!--- row -->

         </div> <!--- container -->
    </div>

 </main>

<?php endwhile; ?>

<?php get_footer() ?>
