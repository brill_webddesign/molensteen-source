<?php get_header() ?>

<main>

<?php while ( have_posts() ) : the_post(); ?>

    <div class="container  ms-c-first-container">

        <div class="row">
            <div class="col-md-6 text-center  ms-c-recipe__page">
                <?php the_post_thumbnail('recipe-thumb'); ?>
            </div>
            <div class="col-md-6">
                <div class="ms-c-product-content">
                    <h1><?php the_title(); ?></h1>
                    <span class="ms-c-volume"><?php the_field('time') ?></span>
                    <?php the_content(); ?>
                    <?php if ( get_field( 'biologisch' ) == 1 ) : ?>
                        <span class="ms-c-bio">Biologisch</span>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>

    <div class="container ms-u-bg--terracotta-light ms-u-product-info ms-c--<?php the_field('color'); ?>">
        <div class="row">
            <div class="col-md-6">
                <h4>Ingredi&euml;nten</h4>
                <?php the_field('ingredienten'); ?>
            </div>
            <div class="col-md-6">
                <h4>Recept</h4>
                <?php the_field('recept'); ?>
            </div>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer() ?>
