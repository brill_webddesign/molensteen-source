<?php

require_once('includes/shortcodes.php');
require_once('includes/wordpress-typekit-enqueue.php');
require_once('includes/front-end-functions.php');
require_once('includes/versioning.php');

/* Enqueue Theme Styles */
function bd_theme_styles() {

	wp_enqueue_style( 'dashicons' );
	wp_enqueue_style( 'animations', get_template_directory_uri() . '/assets/css/animations.css' , '', '1.0' );
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/assets/css/owl.carousel.min.css','','1.0' );
	wp_enqueue_style( 'bd_theme-style', BD_THEME_STYLE_VERSIONING );
}

add_action( 'wp_enqueue_scripts', 'bd_theme_styles' );

/* Enqueue Theme scripts */
function bd_theme_js() {

	wp_enqueue_script( 'owl', get_template_directory_uri() . '/assets/js/lib/owl.carousel.min.js', array('jquery'), '1.0', true );
	wp_enqueue_script( 'animate_it', get_template_directory_uri() . '/assets/js/css3-animate-it.js?v=' . time() , array('jquery'), '0.1', true );
	wp_enqueue_script( 'parallax', get_template_directory_uri() . '/assets/js/lib/parallax.min.js', array('jquery'), '1.0', true );
	wp_enqueue_script( 'main_js', get_template_directory_uri() . '/assets/js/scripts.js?v=' . time() , array('jquery'), '0.1', true );
	wp_localize_script( 'main_js', 'my_ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ));
}

add_action( 'wp_enqueue_scripts', 'bd_theme_js' );

/* Add Theme Support for Menu's */
add_theme_support('menus');

// Register Nav Menus
function bd_register_menus() {
	register_nav_menus( array(
        'main-nav-left' => "Main Menu Left",
		'main-nav-right' => "Main Menu Right",
        'footer-nav' => "Footer Menu"
    ) );
}

add_action( 'init', 'bd_register_menus' );

/**********************************************************************************
bd_widgets_init - register widgets
**********************************************************************************/
function bd_widgets_init() {

	// menu footer widget area
	register_sidebar (array(
		'name'          => __('Footer Social','rm'),
		'id'            => "footer-social",
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '' )
		);

	// contact footer widget area
	register_sidebar (array(
		'name'          => __('Footer Contact','rm'),
		'id'            => "footer-contact",
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '' )
		);

}

add_action('init', 'bd_widgets_init');

/* Post Thumbnail Support */
if (function_exists('add_theme_support')) {
	add_theme_support('post-thumbnails');
        set_post_thumbnail_size(150,150);
}

/* Add image sizes  (name, width, height, crop) */
if (function_exists('add_image_size') ) {
    add_image_size('product-thumb',500,500,false);
	add_image_size('recipe-loop',391,220,true);
	add_image_size('recipe-thumb',600,600,true);
}

/* Hide all upgrades from non-admins (Core, Themes and Plugins ------------------------------------------------------------ */
global $current_user;
wp_get_current_user();
if ($current_user->user_login != "_brilldigital") {
	add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );
	add_filter( 'pre_option_update_core', create_function( '$a', "return null;" ) );
	remove_action('admin_notices', 'update_nag', 3);
	remove_action( 'load-update-core.php', 'wp_update_plugins' );
	add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );
}


/* Remove the Core updates from Dashboards for non-admins */
add_action( 'admin_menu', 'adjust_the_wp_menu', 999 );
function adjust_the_wp_menu() {
    global $current_user;
    wp_get_current_user();
    if($current_user->user_login != '_brilldigital')
    {
		$page = remove_submenu_page('index.php', 'update-core.php');
  }
}

/* Remove WP Node */
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );
function remove_wp_logo( $wp_admin_bar ) {
    $wp_admin_bar->remove_node('wp-logo');
}


/* Change WP Admin CSS ------------------------------------------------------------ */
function bd_custom_css() {
    global $menu;
    global $current_user;
    if($current_user->user_login != '_brilldigital') {
	  echo
	    '<style type="text/css">
			#toplevel_page_wpcf7,
			#toplevel_page_edit-post_type-acf,
			#toplevel_page_meowapps-main-menu,
			#toplevel_page_cptui_main_menu,
			#wp-admin-bar-new-content,
			#wp-admin-bar-updates,
			#toplevel_page_maintenance,
			#toplevel_page_woocommerce,
			#toplevel_page_mlang,
			#toplevel_page_mailchimp-for-wp,
			#toplevel_page_meowapps-main-menu,
			#toplevel_page_aiowpsec
		 {display:none !important;}
	    </style>';
    }
}

add_action('admin_head', 'bd_custom_css');


/* Remove some menu's for non-admins ------------------------------------------------------------ */
function bd_remove_menus()
{
    global $menu;
    global $current_user;
    get_currentuserinfo();
    if($current_user->user_login != '_brilldigital') {
	//if( !current_user_can('administrator') ) {

        $restricted = array(
            __('Links'),
            __('Comments'),
			//__('Appearance'),
            __('Plugins'),
            __('Users'),
            __('Tools'),
			__('Media'),
            __('Settings'),
        );
        end ($menu);
        while (prev($menu)){
            $value = explode(' ',$menu[key($menu)][0]);
            if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){
            	unset($menu[key($menu)]);
            }
        }
    }
}

add_action('admin_menu', 'bd_remove_menus');
