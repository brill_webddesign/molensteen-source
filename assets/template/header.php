<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title(); ?></title>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>

        <header class="animatedParent">
            <nav>
                <?php
                $args = array(
                    'menu' => 'Main Menu Left',
                    'container' => 'ul',
                    'menu_class' => 'animated fadeInDownShort'
                 );
                 wp_nav_menu( $args );
                 ?>
                <div class="ms-c-header__logo animated fadeInDownShort">
                    <a href="<?php bloginfo('url'); ?>" class="ms-c-logo__desktop"><span></span></a>
                </div>
                <?php
                $args = array(
                    'menu' => 'Main Menu Right',
                    'container' => 'ul',
                    'menu_class' => 'animated fadeInDownShort'
                 );
                 wp_nav_menu( $args );
                 ?>
            </nav>
            <a href="<?php bloginfo('url'); ?>" class="ms-c-logo__mobile"><span></span></a>
            <a href="#" class="ms-c-toggle-menu  "><span></span></a>
        </header>
