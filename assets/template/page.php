<?php get_header() ?>

<?php while ( have_posts() ) : the_post(); ?>

    <main>

        <?php get_template_part('partials/partial', 'header' ); ?>

        <?php bd_page_blocks(); ?>

    </main>

<?php endwhile; ?>

<?php get_footer() ?>
