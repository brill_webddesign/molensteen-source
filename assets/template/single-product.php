<?php get_header() ?>

<main>

<?php while ( have_posts() ) : the_post(); ?>

    <div class="container ms-c-first-container ms-c--<?php the_field('color'); ?>">

        <div class="row">
            <div class="col-md-6 ms-c-product-image-background-container">
                <div class="ms-c-product-image-background">
                    <?php the_post_thumbnail('product-thumb'); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="ms-c-product-content">
                    <h1><?php the_title(); ?></h1>
                    <span class="ms-c-volume"><?php the_field('volume') ?></span>
                    <?php the_content(); ?>
                    <?php if ( get_field( 'biologisch' ) == 1 ) : ?>
                        <span class="ms-c-bio">Biologisch</span>
                    <?php endif; ?>

                </div>
            </div>
        </div>

    </div>

    <div class="container ms-u-bg--blue-light ms-u-product-info ms-c--<?php the_field('color'); ?>">
        <div class="row">
            <div class="col-md-6">
                <h4>voedingswaarden</h4>
                <?php the_field('voedingswaarden'); ?>
            </div>
            <div class="col-md-6">
                <h4>Ingredi&euml;nten</h4>
                <?php the_field('ingredienten'); ?>

                <?php if( get_field('allergie_info') ) : ?>

                    <div class="ms-c-allergy__container">
                        <h4 class="ms-c-allergy">Allergie-informatie</h4>
                        <?php the_field('allergie_info'); ?>
                    </div>

                <?php endif; ?>

            </div>
        </div>
    </div>

    <?php bd_get_partner_container('Dit product'); ?>

<?php bd_get_product_slider(); ?>

<?php endwhile; ?>

<?php get_footer() ?>
