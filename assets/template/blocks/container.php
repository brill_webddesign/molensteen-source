<div class="<?php echo $container['container_type'] ?>  <?php get_classes($container['container_classes']); ?>  animatedParent">


    <?php if ( $container['container_type'] == 'container-fluid' ): ?>
    <div class="container  <?php if ( $container['animation'] != "none" ) { echo "animated  "; get_classes( $container['animation'] ); } ?>" <?php echo $container['container_attributes']; ?>>
    <?php endif; ?>

    <?php if( $container['rows'] ) : ?>

        <?php foreach ( $container['rows'] as $row) : ?>

            <div class="row animated fadeIn <?php get_classes( $row['row_classes'] );?>" <?php echo $row['row_attributes']; ?>>

                <?php foreach ( $row['columns'] as $column ) : ?>

                    <div class="<?php get_classes( $column['column_classes'] );?> <?php get_classes( $column['column_offset_classes'] );?> <?php get_classes( $column['column_custom_classes'] );?> <?php echo $column['column_custom_classes_user'];?>" <?php echo $column['column_attributes']; ?>>

                        <?php if ( $column['type'] == 'content' ) : ?>

                            <?php echo $column['content']; ?>

                        <?php endif; ?>

                        <?php if ( $column['type'] == 'html' ) : ?>

                            <?php echo $column['html']; ?>

                        <?php endif; ?>

                        <?php if ( $column['type'] == 'image' ) : ?>

                            <?php get_page_block_image( $column ) ; ?>

                        <?php endif; ?>

                        <?php if ( $column['type'] == 'card' ) : ?>

                            <?php include locate_template('blocks/card.php'); ?>

                        <?php endif; ?>

                        <?php if ( $column['type'] == 'video' ) : ?>

                            <?php include locate_template('blocks/video.php'); ?>

                        <?php endif; ?>

                    </div> <!--- column --->

                <?php endforeach; ?>

            </div> <!--- row --->

        <?php endforeach; ?>

    <?php endif; ?>

    <?php if ( $container['container_type'] == 'container-fluid' ): ?>
    </div> <!--- container --->
    <?php endif; ?>

</div> <!--- <?php echo $container['container_type'] ?> --->
