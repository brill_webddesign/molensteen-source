<?php

if ( ! is_page( 'producten') ) {
    $category = get_the_category();

    $current_cat = $category[0]->slug;
    if ( $current_cat != $prev_cat ) {
        // new category
        $data_hash = 'data-hash="' . $current_cat . '"';
    } else {
        $data_hash = '';
    }
    $prev_cat =  $category[0]->slug;
}

 ?>

<a href="<?php the_permalink(); ?>">
    <div class="ms-c-carousel-item  ms-u-bg--<?php the_field('color');?>" <?php echo $data_hash; ?>>
        <?php the_post_thumbnail('product-thumb'); ?><span class="ms-c-product-loop-item__title"><?php the_field('product_short_title'); ?></span>
    </div>
</a>
