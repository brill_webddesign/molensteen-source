jQuery(function ($) {

    console.log( 'everything is awesome! ');

    var action = false, clicked = false;
    var Owl = {
        init: function() {
          Owl.carousel();
        },

    	carousel: function() {
    		var owl;
    		$(document).ready(function() {

    			owl = $('#header_carousel').owlCarousel({
                    items: 1,
                    center: true,
                    lazyLoad:true,
                    loop:true,
                    autoplay: false,
                    dotsContainer: '.ms-c-owl-dots',
                    margin: 0,
    			});

    			  $('.owl-next').on('click',function(){
    			  	action = 'next';
    			  });

    			  $('.owl-prev').on('click',function(){
    			  	action = 'prev';
    			  });

    			 $('.ms-c-owl-dots').on('click', 'li', function(e) {
    			    owl.trigger('to.owl.carousel', [$(this).index(), 300]);
    			  });
    		});
    	}
    };

    $(document).ready(function() {
      Owl.init();
    });

    $('#products_carousel').owlCarousel({
          items: 5,
          loop: true,
          center: true,
          margin: 20,
          callbacks: true,
          URLhashListener: true,
          autoplayHoverPause: true,
          autoHeight:false,
          slideTransition: 'linear',
          autoplayTimeout: 4200,
          autoplaySpeed: 4200,
          autoplay:true,
          dots: false,
          mouseDrag: true,
          responsive : {

                   // breakpoint from 768 up
                   768 : {
                      items: 3,
                      margin: 30
                  },
                   // breakpoint from 1024 up
                   1120 : {
                      items: 4,
                      margin: 10
                   },
                    // breakpoint from 1440 up
                    1640 : {
                       items: 5,
                       margin: 10
                   }
               }
        });

    // Products Carousel

    $('#ms_product_slider_menu li').on('click', function(e){
        $('.ms-c-product-slider-menu li').removeClass('ms-js-is-active');
        $(this).addClass('ms-js-is-active');
    });

    $( window ).on( 'hashchange', function( e ) {
        var menu_item = window.location.hash.substr(1);
        $('.ms-c-product-slider-menu li').removeClass('ms-js-is-active');
        $("#slider_menu_"+menu_item).addClass('ms-js-is-active');
    } );

    // Scroll Add class
    $(window).on('scroll', function(){

        if ( $(window).scrollTop() >= 75 ) {
            $("header").addClass('ms-is-fixed');
        } else {
            $("header").removeClass('ms-is-fixed');
        }

    });


    // Toggle Menu (Mobile Navigation)
    $(".ms-c-toggle-menu").on('click', function(){
        $(this).toggleClass('ms-js-toggle-is-active');
        $('header').toggleClass('ms-js-header-is-active');
    });

    // hide img width and height

    $('img').removeAttr('width');
    $('img').removeAttr('height');

    // toggle product filter menu on mobile screens

    $('.ms-c-toggle-product-filter').on('click', function(){
        $(this).toggleClass('ms-js-is-active');
        $('.ms-c-product-slider-menu ul').toggleClass('ms-c-is-active');
    });

    // Product Filter
    $('#ms_product_filter li').on('click', function(e){
        e.preventDefault();
        $('.ms-c-product-slider-menu ul').removeClass('ms-c-is-active');
        $('.ms-c-product-slider-menu li').removeClass('ms-js-is-active');
        $(this).addClass('ms-js-is-active');
        var category_id = $(this).attr('data-id');
        console.log(category_id);
        loadContent(category_id,'filter_products');
    });

    // Recipe Filter
    $('#ms_recept_filter li').on('click', function(e){
        e.preventDefault();
        $('.ms-c-product-slider-menu ul').removeClass('ms-c-is-active');
        $('.ms-c-product-slider-menu li').removeClass('ms-js-is-active');
        $(this).addClass('ms-js-is-active');
        var category_id = $(this).attr('data-id');
        console.log(category_id);
        loadContent(category_id,'filter_recipes');
    });



    function loadContent( category_id, action ) {

        //console.log(sort_type);
        $('#ms_products_container').html('');
        //$('#motoren_content').addClass('faded');
        $('.ms-c-product__loader').addClass('ms-js-is-active');

        $.ajax({
            url: my_ajax_object.ajaxurl,
            type: 'post',
            data: {
                category_id : category_id,
                action: action
            },
            success: function( data ) {
                if( $('.ms-c-product__loader').hasClass('ms-js-is-active') ) {
                    $('.ms-c-product__loader').removeClass('ms-js-is-active');
                }

                $('#ms_products_container').html(data); // insert data
                $('img').removeAttr('width');
                $('img').removeAttr('height');
                //$('#motoren_content').removeClass('faded');
            }
        });
    }

});
